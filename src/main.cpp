#include "main.h"
#include "okapi/api/util/logging.hpp"
#include "okapi/impl/util/timeUtilFactory.hpp"

/**
 * A callback function for LLEMU's center button.
 *
 * When this callback is fired, it will toggle line 2 of the LCD text between
 * "I was pressed!" and nothing.
 */
void on_center_button()
{
	static bool pressed = false;
	pressed = !pressed;
	if (pressed)
	{
		pros::lcd::set_text(2, "pros is a pain.");
	}
	else
	{
		pros::lcd::clear_line(2);
	}
}

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize()
{
	pros::lcd::initialize();
	initialize_screen();
	pros::lcd::set_text(1, "Hello PROS User!!!");

	pros::lcd::register_btn1_cb(on_center_button);

	okapi::Logger::setDefaultLogger(
    std::make_shared<okapi::Logger>(
        okapi::TimeUtilFactory::createDefault().getTimer(), // It needs a Timer
        "/ser/sout", // Output to the PROS terminal
        okapi::Logger::LogLevel::warn // Show errors and warnings
    )
);
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {}

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */

void opcontrol()
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor left_mtr(3);
	pros::Motor right_mtr(4);

	pros::Motor double_arms1(11);
	pros::Motor double_arms2(12);

	pros::Motor conveyer(10);
	uint8_t conveyer_state = 0b00;

	bool r1buttonstate = 0;
	bool r2buttonstate = 0;

	int speed = 1;

	FILE* usd_file_write = fopen("/usd/test", "w");

	bool isRecording = false;
	bool isReading = false;
	int curLoc = 0;
	std::string buf;

	while (true)
	{
		pros::lcd::print(0, "%d %d %d", (pros::lcd::read_buttons() & LCD_BTN_LEFT) >> 2,
						 (pros::lcd::read_buttons() & LCD_BTN_CENTER) >> 1,
						 (pros::lcd::read_buttons() & LCD_BTN_RIGHT) >> 0);

		
		if(master.get_digital(DIGITAL_Y)){
			isReading = false;
			// if(isRecording == true)
			// 	fclose(usd_file_write);
			isRecording = !isRecording;

			pros::lcd::set_text(1, "Recording changed to " + isRecording);
		}

		int firstnum = 0;
		int secondnum = 0;

		if(master.get_digital(DIGITAL_B)){
			if(isRecording){
				pros::lcd::set_text(2, "Stop recording before reading.");
			}else{
				isReading = true;

 			 	long lSize;
				char * buffer;
				size_t result;

				FILE* usd_file_read = fopen("/usd/test", "r");

				fseek (usd_file_read , 0 , SEEK_END);
				lSize = ftell (usd_file_read);
				rewind (usd_file_read);

				std::string mem = "mem err";
				std::string red = "reading err";
				
				 // allocate memory to contain the whole file:
				buffer = (char*) malloc (sizeof(char)*lSize);
				if (buffer == NULL) {okapi::defaultLogger.get()->warn(mem);}

				// copy the file into the buffer:
				result = fread (buffer,1,lSize,usd_file_read);
				fclose(usd_file_read); // always close files when you're done with them
				if (result != lSize) {okapi::defaultLogger.get()->warn(red);}

				std::string tmp(buffer);

				std::string w = tmp;
				// okapi::defaultLogger.get()->warn(std::to_string(lSize));
				// okapi::defaultLogger.get()->warn(w);
				//okapi::defaultLogger.get()->warn(test);
				/* the whole file is now loaded in the memory buffer. */
				buf = tmp;
				// Should print "Example text" to the terminal
			}
		}

		if(isRecording){
			std::string nLine = std::to_string(master.get_analog(ANALOG_LEFT_X)) + ":" + std::to_string(master.get_analog(ANALOG_LEFT_Y)) + "\n";
			fputs(nLine.c_str(), usd_file_write);
		}else if(isReading){
			okapi::defaultLogger.get()->warn(std::to_string(curLoc));
			int tmp = buf.find(':', curLoc);

			okapi::defaultLogger.get()->warn(std::to_string(buf.find('\n', curLoc + 1)));
			
			firstnum = std::stoi(buf.substr(curLoc, tmp));
			secondnum = std::stoi(buf.substr(tmp + 1, buf.find('\n', curLoc + 1)));
			curLoc = buf.find('\n', curLoc + 1);


			int rotation = (firstnum);
			int volts = (secondnum);

			master.print(2, 0, "Speed: %d", speed);

			const int is_neg_modifer = (volts >= 0 ? -1 : 1);
			left_mtr = right_mtr = volts;
			if (rotation > 0)
			{
				right_mtr = volts + rotation * is_neg_modifer;
			}
			else
			{
				left_mtr = volts - rotation * is_neg_modifer;
			}
		}
		

		double_arms1 = master.get_analog(ANALOG_LEFT_Y);
		double_arms2 = master.get_analog(ANALOG_LEFT_Y);

		bool conveyeronoff = master.get_digital(DIGITAL_R1);
		bool conveyerreverse = master.get_digital(DIGITAL_R2);

		if(conveyeronoff && !r1buttonstate) conveyer_state ^= 0b01;
		if(conveyerreverse && !r2buttonstate) conveyer_state ^= 0b10;

		r1buttonstate = conveyeronoff;
		r2buttonstate = conveyerreverse;

		master.print(2, 0, "Speed: %d", conveyer_state);

		conveyer = ((conveyer_state == 3) ? -1 : 1) * ((conveyer_state & 0b01 == 1) ? 127 : 0);

		int rotation = master.get_analog(ANALOG_LEFT_X);
		int volts = master.get_analog(ANALOG_RIGHT_Y);

		int speedDown = master.get_digital(DIGITAL_L1);
		int speedUp = master.get_digital(DIGITAL_R1);

		if (speedUp == 1)
		{
			speed = 2;
		}
		if (speedDown == 1)
		{
			speed = 1;
		}
		master.print(2, 0, "Speed: %d", speed);

		const int is_neg_modifer = (volts >= 0 ? -1 : 1);
		left_mtr = right_mtr = volts;
		if (rotation > 0)
		{
			right_mtr = volts + rotation * is_neg_modifer;
		}
		else
		{
			left_mtr = volts - rotation * is_neg_modifer;
		}

		pros::delay(100);
	}
}
